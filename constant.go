package bitdna

/*
Should remain constant, but array constants are not allowed
*/
var nucMap = map[rune]byte{
	/*
		Single nucleotides
	*/
	'A': 8, // 0000 1000 -> 8
	'T': 4, // 0000 0100 -> 4
	'G': 2, // 0000 0010 -> 2
	'C': 1, // 0000 0001 -> 1
	/*
		Any of two nucleotides
	*/
	'R': 10, // A | G -> 0000 1010 -> 10
	'Y': 5,  // T | C -> 0000 0101 -> 5
	'S': 3,  // G | C -> 0000 0011 -> 3
	'W': 12, // A | T -> 0000 1100 -> 12
	'K': 6,  // T | G -> 0000 0110 -> 6
	'M': 9,  // A | C -> 0000 1001 -> 9
	/*
		Any of three nucleotides
	*/
	'B': 7,  // T | G | C -> 0000 0111 -> 7
	'D': 14, // A | T | G -> 0000 1110 -> 14
	'H': 13, // A | T | C -> 0000 1101 -> 13
	'V': 11, // A | G | C -> 0000 1011 -> 11
	/*
		Any of four nucleotides
	*/
	'N': 15, // A | T | G | C -> 0000 1111 -> 15
	/*
		None of four nucleotides
	*/
	'_': 0, // A & T & G & C -> 0000 0000 -> 0
}

var reverseMapping = map[rune]rune{
	/*
		Single nucleotides
	*/
	'A': 'T',
	'T': 'A',
	'G': 'C',
	'C': 'G',
	/*
		Any of two nucleotides
	*/
	'R': 'Y',
	'Y': 'R',
	'S': 'S',
	'W': 'W',
	'K': 'M',
	'M': 'K',
	/*
		Any of three nucleotides
	*/
	'B': 'V',
	'D': 'H',
	'H': 'D',
	'V': 'B',
	/*
		Any of four nucleotides
	*/
	'N': 'N',
	/*
		None of four nucleotides
	*/
	'_': '_',
}
