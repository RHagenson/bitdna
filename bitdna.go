package bitdna

import "fmt"
import "strings"
import "bytes"
import ba "bitbucket.org/rhagenson/bitarray"

type DNA struct {
	seq        *ba.BitArray
	nuc_counts map[rune]int
	length     uint
}

func NewDNA(sequence string) *DNA {
	return new(DNA).Init(sequence)
}

func (s *DNA) Init(sequence string) *DNA {
	s.nuc_counts = make(map[rune]int)

	s.seq = ba.NewBitArray(uint(len(sequence)), 4)
	for index, char := range strings.ToUpper(sequence) {
		value, ok := nucMap[char]
		if ok { // Valid IUPAC character
			s.seq.SetWord(uint(index), value)
			s.nuc_counts[char] += 1
		} else if strings.ContainsRune("_-ZX", char) { // Check for the variety of "space" characters
			s.seq.SetWord(uint(index), nucMap['_'])
			s.nuc_counts['_'] += 1
		} else { // Invalid character
			panic(fmt.Sprintf("Invalid DNA character: %q\n", char))
		}
	}
	s.length = uint(len(sequence))

	return s
}

func (s *DNA) GetSeq() string {
	var buffer bytes.Buffer

	for i := uint(0); i < uint(s.length); i++ {
		key, ok := mapkey(nucMap, s.seq.GetWord(i))
		if ok {
			buffer.WriteRune(key)
		} else {
			panic("Could not reverse the map.")
		}
	}

	return buffer.String()
}

func (s *DNA) GetLength() uint {
	return s.length
}

func (s *DNA) ACount() (val int) {
	val = s.ArbitraryCountRune('A')
	return
}

func (s *DNA) TCount() (val int) {
	val = s.ArbitraryCountRune('T')
	return
}

func (s *DNA) GCount() (val int) {
	val = s.ArbitraryCountRune('G')
	return
}

func (s *DNA) CCount() (val int) {
	val= s.ArbitraryCountRune('C')
	return
}

func (s *DNA) GapCount() (val int) {
	val = s.ArbitraryCountRune('_')
	return
}

func (s *DNA) ArbitraryCount(str string) int {
	count := int(0)
	for _, char := range strings.ToUpper(str) {
		val := s.ArbitraryCountRune(rune(char))
		count += val
	}
	return count
}

func (s *DNA) ArbitraryCountRune(r rune) int {
		val, ok := s.nuc_counts[r]
		if ok {
			return val
		} else {
			return 0
		}
}

func (s *DNA) ReverseDNA() *DNA {
	return NewDNA(s.ReverseString())
}

func (s *DNA) ReverseString() string {
	forward := s.GetSeq()
	reverse := []rune(forward)
	for i, j := 0, len(reverse)-1; i < len(reverse)/2; i, j = i+1, j-1 {
		reverse[i], reverse[j] = reverse[j], reverse[i]
	}
	return string(reverse)
}

func (s *DNA) ComplementDNA() *DNA {
	return NewDNA(s.ComplementString())
}

func (s *DNA) ComplementString() string {
	var buffer bytes.Buffer

	for i := uint(0); i < uint(s.length); i++ {
		key, ok := mapkey(nucMap, s.seq.GetWord(i))
		if ok {
			buffer.WriteRune(reverseMapping[key])
		} else {
			panic("Could not reverse the map.")
		}
	}
	return buffer.String()
}

func (s *DNA) Print() string {
	var prt bytes.Buffer

	prt.WriteString(fmt.Sprintln("DNA Sequence:"))
	prt.WriteString(fmt.Sprintf("BitArray values: %b\n", s.seq.GetBytes()[1:10]))
	prt.WriteString(fmt.Sprintf("Sequence start: %s\n", s.GetSeq()[1:20]))
	prt.WriteString(fmt.Sprintf("Length: %d\n", s.GetLength()))
	prt.WriteString(fmt.Sprintf("Adenine count: %d\n", s.ACount()))
	prt.WriteString(fmt.Sprintf("Thymine count: %d\n", s.TCount()))
	prt.WriteString(fmt.Sprintf("Guanine count: %d\n", s.GCount()))
	prt.WriteString(fmt.Sprintf("Cysteine count: %d\n", s.CCount()))
	prt.WriteString(fmt.Sprintf("Gap count: %d\n", s.GapCount()))

	return prt.String()
}
