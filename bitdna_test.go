package bitdna

import (
	"math/rand"
	"strings"
	"testing"
	"testing/quick"
	"time"
)

func TestGaplessMapsBackToOriginalSequence(t *testing.T) {
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(5000, letterBytes)
		dna := NewDNA(sequence)
		return dna.GetSeq() == strings.ToUpper(sequence)
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error(err)
	}
}

func TestGapsBecomeIUPACGap(t *testing.T) {
	assertion := func(n int, charset string) bool {
		gaps := RandStringBytesMaskImprSrc(5000, "_-ZzXx")
		dna := NewDNA(gaps)
		return dna.GetSeq() == strings.Repeat("_", len(gaps))
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error(err)
	}
}

func TestLengthAlwaysMatches(t *testing.T) {
	length := uint(5000)
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(int(length), letterBytes)
		dna := NewDNA(sequence)
		return dna.GetLength() == length
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error(err)
	}
}

func TestLengthIsTrue(t *testing.T) {
	length := uint(12)
	dna := NewDNA("ATGCatgc_-Xx")
	if dna.GetLength() != length {
		t.Error("Length is not being calculated correctly.")
	}
}

func TestAdenineCount(t *testing.T) {
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(5000, letterBytes)
		dna := NewDNA(sequence)
		return dna.ACount() == int(strings.Count(sequence, "a")+strings.Count(sequence, "A"))
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error(err)
	}
}

func TestThymineCount(t *testing.T) {
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(5000, letterBytes)
		dna := NewDNA(sequence)
		return dna.TCount() == int(strings.Count(sequence, "t")+strings.Count(sequence, "T"))
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error(err)
	}
}

func TestGuanineCount(t *testing.T) {
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(5000, letterBytes)
		dna := NewDNA(sequence)
		return dna.GCount() == int(strings.Count(sequence, "g")+strings.Count(sequence, "G"))
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error(err)
	}
}

func TestCysteineCount(t *testing.T) {
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(5000, letterBytes)
		dna := NewDNA(sequence)
		return dna.CCount() == int(strings.Count(sequence, "c")+strings.Count(sequence, "C"))
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error(err)
	}
}

func TestGapCount(t *testing.T) {
	assertion := func(n int, charset string) bool {
		gaps := RandStringBytesMaskImprSrc(5000, "_-ZzXx")
		dna := NewDNA(gaps)
		return dna.GapCount() == int(len(gaps))
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error(err)
	}
}

func TestArbitraryCount(t *testing.T) {
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(5000, letterBytes)
		mini_seq := uniq([]byte(RandStringBytesMaskImprSrc(5, letterBytes)))
		dna := NewDNA(sequence)
		true_count := 0
		for _, val := range mini_seq {
			true_count += strings.Count(strings.ToUpper(sequence), strings.ToUpper(string(val)))
		}
		return dna.ArbitraryCount(mini_seq) == int(true_count)
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error(err)
	}
}

func TestDoubleReverseDNAIsOriginalDNASimple(t *testing.T) {
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(5000, letterBytes)
		dna := NewDNA(sequence)
		rev_rev_dna := dna.ReverseDNA().ReverseDNA()
		return dna.GetSeq() == rev_rev_dna.GetSeq()
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error("Double reverse did not yield the original sequence.")
	}
}

func TestReverseComplementRevereComplementIsOriginalDNA(t *testing.T) {
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(5000, letterBytes)
		dna := NewDNA(sequence)
		rev_rev_dna := dna.ComplementDNA().ComplementDNA()
		return dna.GetSeq() == rev_rev_dna.GetSeq()
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error("Double reverse complement did not yield the original sequence.")
	}

}

func TestPrintFormatWithColons(t *testing.T) {
	assertion := func(n int, charset string) bool {
		sequence := RandStringBytesMaskImprSrc(5000, letterBytes)
		dna := NewDNA(sequence)
		print_string := dna.Print()
		return strings.Count(print_string, ":") == strings.Count(print_string, "\n")
	}
	if err := quick.Check(assertion, nil); err != nil {
		t.Error("The DNA print statement may be too complex")
	}
}

/*
Prepare function to create random strings from charactersetvar src = rand.NewSource(time.Now().UnixNano())
Function taken from https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-golang
*/
const letterBytes = "atgcryswkmbdhvnATGCRYSWKMBDHVN"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

func RandStringBytesMaskImprSrc(n int, charset string) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < int(len(charset)) {
			b[i] = charset[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}
	return string(b)
}

func uniq(bytes []byte) string {
	unique_set := make(map[byte]bool, len(bytes))
	for _, x := range bytes {
		unique_set[x] = true
	}

	result := make([]byte, 0, len(unique_set))
	for x := range unique_set {
		result = append(result, x)
	}
	return string(result)
}
