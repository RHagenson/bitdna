package bitdna

/*
Helper function to reverse map values to keys
*/
func mapkey(m map[rune]byte, value byte) (key rune, ok bool) {
	for k, v := range m {
		if v == value {
			key = k
			ok = true
			return
		}
	}
	return
}
